
# sent_id = weblog-juancole.com_juancole_20051126063000_ENG_20051126_063000-0001
# text = Al-Zaman : American forces killed Shaikh Abdullah al-Ani, the preacher at the mosque in the town of Qaim, near the Syrian border.
1	Al	Al	PROPN	NN	Number=Sing	0	root	_	_
2	-	-	PUNCT	:	_	1	punct	_	_
3	Zaman	Zaman	PROPN	NNP	Number=Sing	1	flat	_	_
4	:	:	PUNCT	:	_	1	punct	_	_
5	American	american	ADJ	JJ	Degree=Pos	6	amod	_	_
6	forces	force	NOUN	NNS	Number=Plur	7	nsubj	_	_
7	killed	kill	VERB	VBD	Mood=Ind|Tense=Past|VerbForm=Fin	1	parataxis	_	_
8	Shaikh	Shaikh	PROPN	NNP	Number=Sing	7	obj	_	_
9	Abdullah	Abdullah	PROPN	NNP	Number=Sing	8	flat	_	_
10	al	al	PROPN	NNP	Number=Sing	8	flat	_	_
11	-	-	PUNCT	:	_	8	punct	_	_
12	Ani	Ani	PROPN	NNP	Number=Sing	8	flat	_	_
13	,	,	PUNCT	,	_	8	punct	_	_
14	the	the	DET	DT	Definite=Def|PronType=Art	15	det	_	_
15	preacher	preacher	NOUN	NN	Number=Sing	8	appos	_	_
16	at	at	ADP	IN	_	18	case	_	_
17	the	the	DET	DT	Definite=Def|PronType=Art	18	det	_	_
18	mosque	mosque	NOUN	NN	Number=Sing	7	obl	_	_
19	in	in	ADP	IN	_	21	case	_	_
20	the	the	DET	DT	Definite=Def|PronType=Art	21	det	_	_
21	town	town	NOUN	NN	Number=Sing	18	nmod	_	_
22	of	of	ADP	IN	_	23	case	_	_
23	Qaim	Qaim	PROPN	NNP	Number=Sing	21	nmod	_	_
24	,	,	PUNCT	,	_	21	punct	_	_
25	near	near	ADP	IN	_	28	case	_	_
26	the	the	DET	DT	Definite=Def|PronType=Art	28	det	_	_
27	Syrian	syrian	ADJ	JJ	Degree=Pos	28	amod	_	_
28	border	border	NOUN	NN	Number=Sing	21	nmod	_	_
29	.	.	PUNCT	.	_	1	punct	_	_

# sent_id = weblog-juancole.com_juancole_20051126063000_ENG_20051126_063000-0016
# text = This willingness is the main difference in the number of bombings in the south as opposed to the center-north of the country.)
1	This	this	DET	DT	Number=Sing|PronType=Dem	2	det	_	_
2	willingness	willingness	NOUN	NN	Number=Sing	6	nsubj	_	_
3	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	6	cop	_	_
4	the	the	DET	DT	Definite=Def|PronType=Art	6	det	_	_
5	main	main	ADJ	JJ	Degree=Pos	6	amod	_	_
6	difference	difference	NOUN	NN	Number=Sing	0	root	_	_
7	in	in	ADP	IN	_	9	case	_	_
8	the	the	DET	DT	Definite=Def|PronType=Art	9	det	_	_
9	number	number	NOUN	NN	Number=Sing	6	nmod	_	_
10	of	of	ADP	IN	_	11	case	_	_
11	bombings	bombing	NOUN	NNS	Number=Plur	9	nmod	_	_
12	in	in	ADP	IN	_	14	case	_	_
13	the	the	DET	DT	Definite=Def|PronType=Art	14	det	_	_
14	south	south	NOUN	NN	Number=Sing	11	nmod	_	_
15	as	as	SCONJ	IN	_	21	case	_	_
16	opposed	oppose	VERB	VBN	Tense=Past|VerbForm=Part	15	fixed	_	_
17	to	to	ADP	IN	_	15	fixed	_	_
18	the	the	DET	DT	Definite=Def|PronType=Art	21	det	_	_
19	center	center	NOUN	NN	Number=Sing	21	compound	_	_
20	-	-	PUNCT	:	_	21	punct	_	_
21	north	north	NOUN	NN	Number=Sing	14	nmod	_	_
22	of	of	ADP	IN	_	24	case	_	_
23	the	the	DET	DT	Definite=Def|PronType=Art	24	det	_	_
24	country	country	NOUN	NN	Number=Sing	21	nmod	_	_
25	.	.	PUNCT	.	_	6	punct	_	_
26	)	)	PUNCT	)	_	6	punct	_	_

# sent_id = weblog-juancole.com_juancole_20051126063000_ENG_20051126_063000-0025
# text = (This is a largely Sunni Arab clan, and some Sunni observers have accused Shiite elements in the government of being behind the assassination; it is more likely the work of Sunni Arab guerrillas punishing the Batawi leaders for cooperating with the Dec. 15 elections.)
1	(	(	PUNCT	(	_	8	punct	_	_
2	This	this	PRON	DT	Number=Sing|PronType=Dem	8	nsubj	_	_
3	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	8	cop	_	_
4	a	a	DET	DT	Definite=Ind|PronType=Art	8	det	_	_
5	largely	largely	ADV	RB	_	8	advmod	_	_
6	Sunni	sunni	ADJ	JJ	Degree=Pos	8	amod	_	_
7	Arab	arab	ADJ	JJ	Degree=Pos	8	amod	_	_
8	clan	clan	NOUN	NN	Number=Sing	0	root	_	_
9	,	,	PUNCT	,	_	15	punct	_	_
10	and	and	CCONJ	CC	_	15	cc	_	_
11	some	some	DET	QT	_	13	det	_	_
12	Sunni	sunni	ADJ	JJ	Degree=Pos	13	amod	_	_
13	observers	observer	NOUN	NNS	Number=Plur	15	nsubj	_	_
14	have	have	AUX	VBP	Mood=Ind|Tense=Pres|VerbForm=Fin	15	aux	_	_
15	accused	accuse	VERB	VBN	Tense=Past|VerbForm=Part	8	conj	_	_
16	Shiite	shiite	ADJ	JJ	Degree=Pos	17	amod	_	_
17	elements	element	NOUN	NNS	Number=Plur	15	obj	_	_
18	in	in	ADP	IN	_	20	case	_	_
19	the	the	DET	DT	Definite=Def|PronType=Art	20	det	_	_
20	government	government	NOUN	NN	Number=Sing	17	nmod	_	_
21	of	of	SCONJ	IN	_	25	mark	_	_
22	being	be	AUX	VBG	VerbForm=Ger	25	cop	_	_
23	behind	behind	ADP	IN	_	25	case	_	_
24	the	the	DET	DT	Definite=Def|PronType=Art	25	det	_	_
25	assassination	assassination	NOUN	NN	Number=Sing	15	advcl	_	_
26	;	;	PUNCT	,	_	8	punct	_	_
27	it	it	PRON	PRP	Case=Nom|Gender=Neut|Number=Sing|Person=3|PronType=Prs	32	nsubj	_	_
28	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	32	cop	_	_
29	more	more	ADV	RBR	_	30	advmod	_	_
30	likely	likely	ADV	RB	_	32	advmod	_	_
31	the	the	DET	DT	Definite=Def|PronType=Art	32	det	_	_
32	work	work	NOUN	NN	Number=Sing	8	parataxis	_	_
33	of	of	ADP	IN	_	36	case	_	_
34	Sunni	sunni	ADJ	JJ	Degree=Pos	36	amod	_	_
35	Arab	arab	ADJ	JJ	Degree=Pos	36	amod	_	_
36	guerrillas	guerrilla	NOUN	NNS	Number=Plur	32	nmod	_	_
37	punishing	punish	VERB	VBG	VerbForm=Ger	36	acl	_	_
38	the	the	DET	DT	Definite=Def|PronType=Art	39	det	_	_
39	Batawi	Batawi	PROPN	NNP	Number=Sing	37	obj	_	_
40	leaders	leader	NOUN	NNS	Number=Plur	39	flat	_	_
41	for	for	SCONJ	IN	_	42	mark	_	_
42	cooperating	cooperate	VERB	VBG	VerbForm=Ger	39	acl	_	_
43	with	with	ADP	IN	_	47	case	_	_
44	the	the	DET	DT	Definite=Def|PronType=Art	47	det	_	_
45	Dec.	Dec.	PROPN	NNP	Number=Sing	47	compound	_	_
46	15	15	NUM	CD	NumType=Card	45	nummod	_	_
47	elections	election	NOUN	NNS	Number=Plur	42	obl	_	_
48	.	.	PUNCT	.	_	8	punct	_	_
49	)	)	PUNCT	)	_	8	punct	_	_

# sent_id = weblog-juancole.com_juancole_20051126063000_ENG_20051126_063000-0028
# text = This item is a small one and easily missed.
1	This	this	DET	DT	Number=Sing|PronType=Dem	2	det	_	_
2	item	item	NOUN	NN	Number=Sing	6	nsubj	_	_
3	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	6	cop	_	_
4	a	a	DET	DT	Definite=Ind|PronType=Art	6	det	_	_
5	small	small	ADJ	JJ	Degree=Pos	6	amod	_	_
6	one	one	NOUN	NN	Number=Sing	0	root	_	_
7	and	and	CCONJ	CC	_	9	cc	_	_
8	easily	easily	ADV	RB	_	9	advmod	_	_
9	missed	miss	VERB	VBN	Tense=Past|VerbForm=Part	6	conj	_	_
10	.	.	PUNCT	.	_	6	punct	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0003
# text = UPDATE:
1	UPDATE	update	NOUN	NN	Number=Sing	0	root	_	_
2	:	:	PUNCT	:	_	1	punct	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0005
# text = This is not a post about fault-finding or assigning blame.
1	This	this	PRON	DT	Number=Sing|PronType=Dem	5	nsubj	_	_
2	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	5	cop	_	_
3	not	not	PART	RB	_	5	advmod	_	_
4	a	a	DET	DT	Definite=Ind|PronType=Art	5	det	_	_
5	post	post	NOUN	NN	Number=Sing	0	root	_	_
6	about	about	ADP	IN	_	7	case	_	_
7	fault-finding	finding	NOUN	NN	Number=Sing	5	nmod	_	_
8	or	or	CCONJ	CC	_	9	cc	_	_
9	assigning	assign	VERB	VBG	VerbForm=Ger	7	conj	_	_
10	blame	blame	NOUN	NN	Number=Sing	9	obj	_	_
11	.	.	PUNCT	.	_	5	punct	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0006
# text = It is a time to learn what happened and how it may affect the future.
1	It	it	PRON	PRP	Case=Nom|Gender=Neut|Number=Sing|Person=3|PronType=Prs	4	nsubj	_	_
2	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	4	cop	_	_
3	a	a	DET	DT	Definite=Ind|PronType=Art	4	det	_	_
4	time	time	NOUN	NN	Number=Sing	0	root	_	_
5	to	to	PART	TO	_	6	mark	_	_
6	learn	learn	VERB	VB	VerbForm=Inf	4	acl	_	_
7	what	what	PRON	WP	PronType=Int	8	nsubj	_	_
8	happened	happen	VERB	VBD	Mood=Ind|Tense=Past|VerbForm=Fin	6	ccomp	_	_
9	and	and	CCONJ	CC	_	13	cc	_	_
10	how	how	ADV	WRB	PronType=Int	13	advmod	_	_
11	it	it	PRON	PRP	Case=Nom|Gender=Neut|Number=Sing|Person=3|PronType=Prs	13	nsubj	_	_
12	may	may	AUX	MD	VerbForm=Fin	13	aux	_	_
13	affect	affect	VERB	VB	VerbForm=Inf	8	conj	_	_
14	the	the	DET	DT	Definite=Def|PronType=Art	15	det	_	_
15	future	future	NOUN	NN	Number=Sing	13	obj	_	_
16	.	.	PUNCT	.	_	4	punct	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0011
# text = CHERNOBYL ACCIDENT: TEN YEARS ON
1	CHERNOBYL	CHERNOBYL	PROPN	NNP	Number=Sing	2	compound	_	_
2	ACCIDENT	accident	NOUN	NN	Number=Sing	0	root	_	_
3	:	:	PUNCT	:	_	2	punct	_	_
4	TEN	ten	NUM	CD	NumType=Card	5	nummod	_	_
5	YEARS	year	NOUN	NNS	Number=Plur	2	parataxis	_	_
6	ON	on	ADV	RB	_	5	advmod	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0017
# text = ...a drop in the birth rate, a deterioration in women's reproductive health, an increase in complications during pregnancy and birth, and a deterioration in neonatal health....
1	...	...	PUNCT	,	_	3	punct	_	_
2	a	a	DET	DT	Definite=Ind|PronType=Art	3	det	_	_
3	drop	drop	NOUN	NN	Number=Sing	0	root	_	_
4	in	in	ADP	IN	_	7	case	_	_
5	the	the	DET	DT	Definite=Def|PronType=Art	7	det	_	_
6	birth	birth	NOUN	NN	Number=Sing	7	compound	_	_
7	rate	rate	NOUN	NN	Number=Sing	3	nmod	_	_
8	,	,	PUNCT	,	_	10	punct	_	_
9	a	a	DET	DT	Definite=Ind|PronType=Art	10	det	_	_
10	deterioration	deterioration	NOUN	NN	Number=Sing	3	conj	_	_
11	in	in	ADP	IN	_	15	case	_	_
12	women	woman	NOUN	NNS	Number=Plur	15	nmod:poss	_	_
13	's	's	PART	POS	_	12	case	_	_
14	reproductive	reproductive	ADJ	JJ	Degree=Pos	15	amod	_	_
15	health	health	NOUN	NN	Number=Sing	10	nmod	_	_
16	,	,	PUNCT	,	_	18	punct	_	_
17	an	a	DET	DT	Definite=Ind|PronType=Art	18	det	_	_
18	increase	increase	NOUN	NN	Number=Sing	3	conj	_	_
19	in	in	ADP	IN	_	20	case	_	_
20	complications	complication	NOUN	NNS	Number=Plur	18	nmod	_	_
21	during	during	ADP	IN	_	22	case	_	_
22	pregnancy	pregnancy	NOUN	NN	Number=Sing	20	nmod	_	_
23	and	and	CCONJ	CC	_	24	cc	_	_
24	birth	birth	NOUN	NN	Number=Sing	22	conj	_	_
25	,	,	PUNCT	,	_	28	punct	_	_
26	and	and	CCONJ	CC	_	28	cc	_	_
27	a	a	DET	DT	Definite=Ind|PronType=Art	28	det	_	_
28	deterioration	deterioration	NOUN	NN	Number=Sing	3	conj	_	_
29	in	in	ADP	IN	_	31	case	_	_
30	neonatal	neonatal	ADJ	JJ	Degree=Pos	31	amod	_	_
31	health	health	NOUN	NN	Number=Sing	28	nmod	_	_
32	....	....	PUNCT	,	_	3	punct	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0025
# text = IAEA Report Lessons learned
1	IAEA	IAEA	PROPN	NNP	Number=Sing	2	compound	_	_
2	Report	report	NOUN	NN	Number=Sing	0	root	_	_
3	Lessons	Lessons	PROPN	NNPS	Number=Plur	2	appos	_	_
4	learned	learn	VERB	VBN	Tense=Past|VerbForm=Part	3	acl	_	_

# sent_id = weblog-typepad.com_ripples_20040407125600_ENG_20040407_125600-0052
# text = radiation:
1	radiation	radiation	NOUN	NN	Number=Sing	0	root	_	_
2	:	:	PUNCT	:	_	1	punct	_	_

# sent_id = weblog-juancole.com_juancole_20030911085700_ENG_20030911_085700-0002
# text = Because the US and Pakistan have managed to capture or kill about 2/3s of the top 25 al-Qaeda commanders, the middle managers are not in close contact with al-Zawahiri and Bin Laden.
1	Because	because	SCONJ	IN	_	7	mark	_	_
2	the	the	DET	DT	Definite=Def|PronType=Art	3	det	_	_
3	US	US	PROPN	NNP	Number=Sing	7	nsubj	_	_
4	and	and	CCONJ	CC	_	5	cc	_	_
5	Pakistan	Pakistan	PROPN	NNP	Number=Sing	3	conj	_	_
6	have	have	AUX	VBP	Mood=Ind|Tense=Pres|VerbForm=Fin	7	aux	_	_
7	managed	manage	VERB	VBN	Tense=Past|VerbForm=Part	32	advcl	_	_
8	to	to	PART	TO	_	9	mark	_	_
9	capture	capture	VERB	VB	VerbForm=Inf	7	xcomp	_	_
10	or	or	CCONJ	CC	_	11	cc	_	_
11	kill	kill	VERB	VB	VerbForm=Inf	9	conj	_	_
12	about	about	ADV	RB	_	15	advmod	_	_
13	2	2	NUM	CD	NumType=Card	15	compound	_	_
14	/	/	PUNCT	:	_	15	punct	_	_
15	3s	3	NOUN	NNS	Number=Plur	9	obj	_	_
16	of	of	ADP	IN	_	23	case	_	_
17	the	the	DET	DT	Definite=Def|PronType=Art	23	det	_	_
18	top	top	ADJ	JJ	Degree=Pos	23	amod	_	_
19	25	25	NUM	CD	NumType=Card	23	nummod	_	_
20	al	al	PROPN	NNP	Number=Sing	22	compound	_	_
21	-	-	PUNCT	:	_	22	punct	_	_
22	Qaeda	Qaeda	PROPN	NNP	Number=Sing	23	compound	_	_
23	commanders	commander	NOUN	NNS	Number=Plur	15	nmod	_	_
24	,	,	PUNCT	,	_	32	punct	_	_
25	the	the	DET	DT	Definite=Def|PronType=Art	27	det	_	_
26	middle	middle	ADJ	JJ	Degree=Pos	27	amod	_	_
27	managers	manager	NOUN	NNS	Number=Plur	32	nsubj	_	_
28	are	be	AUX	VBP	Mood=Ind|Tense=Pres|VerbForm=Fin	32	cop	_	_
29	not	not	PART	RB	_	32	advmod	_	_
30	in	in	ADP	IN	_	32	case	_	_
31	close	close	ADJ	JJ	Degree=Pos	32	amod	_	_
32	contact	contact	NOUN	NN	Number=Sing	0	root	_	_
33	with	with	ADP	IN	_	36	case	_	_
34	al	al	PROPN	NNP	Number=Sing	36	compound	_	_
35	-	-	PUNCT	:	_	36	punct	_	_
36	Zawahiri	Zawahiri	PROPN	NNP	Number=Sing	32	nmod	_	_
37	and	and	CCONJ	CC	_	38	cc	_	_
38	Bin	Bin	PROPN	NNP	Number=Sing	36	conj	_	_
39	Laden	Laden	PROPN	NNP	Number=Sing	38	flat	_	_
40	.	.	PUNCT	.	_	32	punct	_	_

# sent_id = weblog-juancole.com_juancole_20030911085700_ENG_20030911_085700-0003
# text = The tape was a way to signal priorities.
1	The	the	DET	DT	Definite=Def|PronType=Art	2	det	_	_
2	tape	tape	NOUN	NN	Number=Sing	5	nsubj	_	_
3	was	be	AUX	VBD	Mood=Ind|Number=Sing|Person=3|Tense=Past|VerbForm=Fin	5	cop	_	_
4	a	a	DET	DT	Definite=Ind|PronType=Art	5	det	_	_
5	way	way	NOUN	NN	Number=Sing	0	root	_	_
6	to	to	PART	TO	_	7	mark	_	_
7	signal	signal	VERB	VB	VerbForm=Inf	5	acl	_	_
8	priorities	priority	NOUN	NNS	Number=Plur	7	obj	_	_
9	.	.	PUNCT	.	_	5	punct	_	_

# sent_id = weblog-juancole.com_juancole_20030911085700_ENG_20030911_085700-0013
# text = Response: The US must do whatever it can to strengthen the legitimacy of the Pakistani government.
1	Response	response	NOUN	NN	Number=Sing	0	root	_	_
2	:	:	PUNCT	:	_	1	punct	_	_
3	The	the	DET	DT	Definite=Def|PronType=Art	4	det	_	_
4	US	US	PROPN	NNP	Number=Sing	6	nsubj	_	_
5	must	must	AUX	MD	VerbForm=Fin	6	aux	_	_
6	do	do	VERB	VB	VerbForm=Inf	1	appos	_	_
7	whatever	whatever	PRON	WP	PronType=Int	6	obj	_	_
8	it	it	PRON	PRP	Case=Nom|Gender=Neut|Number=Sing|Person=3|PronType=Prs	9	nsubj	_	_
9	can	can	AUX	MD	VerbForm=Fin	7	acl:relcl	_	_
10	to	to	PART	TO	_	11	mark	_	_
11	strengthen	strengthen	VERB	VB	VerbForm=Inf	6	advcl	_	_
12	the	the	DET	DT	Definite=Def|PronType=Art	13	det	_	_
13	legitimacy	legitimacy	NOUN	NN	Number=Sing	11	obj	_	_
14	of	of	ADP	IN	_	17	case	_	_
15	the	the	DET	DT	Definite=Def|PronType=Art	17	det	_	_
16	Pakistani	pakistani	ADJ	JJ	Degree=Pos	17	amod	_	_
17	government	government	NOUN	NN	Number=Sing	13	nmod	_	_
18	.	.	PUNCT	.	_	1	punct	_	_

# sent_id = weblog-juancole.com_juancole_20030911085700_ENG_20030911_085700-0020
# text = Response: The US has succeeded in politically isolating Hamas, and started the process of cutting off its funding.
1	Response	response	NOUN	NN	Number=Sing	0	root	_	_
2	:	:	PUNCT	:	_	1	punct	_	_
3	The	the	DET	DT	Definite=Def|PronType=Art	4	det	_	_
4	US	US	PROPN	NNP	Number=Sing	6	nsubj	_	_
5	has	have	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	6	aux	_	_
6	succeeded	succeed	VERB	VBN	Tense=Past|VerbForm=Part	1	appos	_	_
7	in	in	SCONJ	IN	_	9	mark	_	_
8	politically	politically	ADV	RB	_	9	advmod	_	_
9	isolating	isolate	VERB	VBG	VerbForm=Ger	6	advcl	_	_
10	Hamas	Hamas	PROPN	NNP	Number=Sing	9	obj	_	_
11	,	,	PUNCT	,	_	13	punct	_	_
12	and	and	CCONJ	CC	_	13	cc	_	_
13	started	start	VERB	VBN	Tense=Past|VerbForm=Part	6	conj	_	_
14	the	the	DET	DT	Definite=Def|PronType=Art	15	det	_	_
15	process	process	NOUN	NN	Number=Sing	13	obj	_	_
16	of	of	SCONJ	IN	_	17	mark	_	_
17	cutting	cut	VERB	VBG	VerbForm=Ger	15	acl	_	_
18	off	off	ADP	RP	_	17	compound:prt	_	_
19	its	its	PRON	PRP$	Gender=Neut|Number=Sing|Person=3|Poss=Yes|PronType=Prs	20	nmod:poss	_	_
20	funding	funding	NOUN	NN	Number=Sing	17	obj	_	_
21	.	.	PUNCT	.	_	1	punct	_	_

# sent_id = weblog-juancole.com_juancole_20030911085700_ENG_20030911_085700-0027
# text = Response: Iraq is actually hostile territory for al-Qaeda, and without Iraqi sympathizers it cannot succeed there.
1	Response	response	NOUN	NN	Number=Sing	0	root	_	_
2	:	:	PUNCT	:	_	1	punct	_	_
3	Iraq	Iraq	PROPN	NNP	Number=Sing	7	nsubj	_	_
4	is	be	AUX	VBZ	Mood=Ind|Number=Sing|Person=3|Tense=Pres|VerbForm=Fin	7	cop	_	_
5	actually	actually	ADV	RB	_	7	advmod	_	_
6	hostile	hostile	ADJ	JJ	Degree=Pos	7	amod	_	_
7	territory	territory	NOUN	NN	Number=Sing	1	appos	_	_
8	for	for	ADP	IN	_	11	case	_	_
9	al	al	PROPN	NNP	Number=Sing	11	compound	_	_
10	-	-	PUNCT	:	_	11	punct	_	_
11	Qaeda	Qaeda	PROPN	NNP	Number=Sing	7	nmod	_	_
12	,	,	PUNCT	,	_	20	punct	_	_
13	and	and	CCONJ	CC	_	20	cc	_	_
14	without	without	ADP	IN	_	16	case	_	_
15	Iraqi	iraqi	ADJ	JJ	Degree=Pos	16	amod	_	_
16	sympathizers	sympathizer	NOUN	NNS	Number=Plur	20	obl	_	_
17	it	it	PRON	PRP	Case=Nom|Gender=Neut|Number=Sing|Person=3|PronType=Prs	20	nsubj	_	_
18	can	can	AUX	MD	VerbForm=Fin	20	aux	_	_
19	not	not	PART	RB	_	20	advmod	_	_
20	succeed	succeed	VERB	VB	VerbForm=Inf	7	conj	_	_
21	there	there	ADV	RB	PronType=Dem	20	advmod	_	_
22	.	.	PUNCT	.	_	1	punct	_	_
